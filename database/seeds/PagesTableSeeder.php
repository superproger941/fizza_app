<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            'name' => 'main'
        ]);
        DB::table('pages')->insert([
            'name' => 'loan'
        ]);
    }
}
