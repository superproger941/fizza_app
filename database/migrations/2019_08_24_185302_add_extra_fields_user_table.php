<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraFieldsUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('address', 512);
            $table->string('ogrn', 256);
            $table->string('company_name', 256);
            $table->string('inn', 256);
            $table->string('number', 256);
            $table->integer('term');
            $table->integer('sum');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('address');
            $table->dropColumn('ogrn');
            $table->dropColumn('company_name');
            $table->dropColumn('inn');
            $table->dropColumn('number');
            $table->dropColumn('term');
            $table->dropColumn('sum');
        });
    }
}
