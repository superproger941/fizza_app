<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('faq');
        Schema::dropIfExists('faqs');

        Schema::create('faqs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 256);
            $table->text('content');
            $table->timestamps();
        });

        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::table('faqs', function (Blueprint $table) {
            $table->integer('page_id')->unsigned();
            $table->foreign('page_id')
                ->references('id')->on('pages')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
        Schema::table('blog_entries', function (Blueprint $table) {
            $table->integer('page_id')->unsigned();
            $table->foreign('page_id')
                ->references('id')->on('pages')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
        Schema::table('reviews', function (Blueprint $table) {
            $table->integer('page_id')->unsigned();
            $table->foreign('page_id')
                ->references('id')->on('pages')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        Schema::dropIfExists('reviews');
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('company');
            $table->text('description');
            $table->longText('img');
            $table->timestamps();
        });
        Schema::table('reviews', function (Blueprint $table) {
            $table->integer('page_id')->unsigned();
            $table->foreign('page_id')
                ->references('id')->on('pages')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('address');
            $table->dropColumn('ogrn');
            $table->dropColumn('company_name');
            $table->dropColumn('inn');
            $table->dropColumn('number');
            $table->dropColumn('term');
            $table->dropColumn('sum');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->string('address', 512)->nullable();
            $table->string('ogrn', 256)->nullable();
            $table->string('company_name', 256)->nullable();
            $table->string('inn', 256)->nullable();
            $table->string('number', 256)->nullable();
            $table->integer('term')->nullable();
            $table->integer('sum')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pages_faqs', function (Blueprint $table) {
            $table->dropForeign(['page_id']);
            $table->dropForeign(['faq_id']);
        });
        Schema::table('pages_blogs_entries', function (Blueprint $table) {
            $table->dropForeign(['page_id']);
            $table->dropForeign(['blog_entrie_id']);
        });
        Schema::table('pages_reviews', function (Blueprint $table) {
            $table->dropForeign(['page_id']);
            $table->dropForeign(['review_id']);
        });
        Schema::dropIfExists('pages_faqs');
        Schema::dropIfExists('pages_blogs_entries');
        Schema::dropIfExists('pages_reviews');

        Schema::dropIfExists('faqs');

        Schema::dropIfExists('pages');
    }
}
