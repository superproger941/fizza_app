<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

/**
 * Class Page
 * @package App\Models
 */
class Page extends Model
{
    use CrudTrait;

    protected $table = 'pages';

    protected $fillable = [
        'name'
    ];
}
