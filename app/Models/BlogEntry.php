<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class BlogEntry extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'blog_entries';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $hidden = [];
    protected $dates = ['publish_after'];

    protected $fillable = [
        'blog',
        'publish_after',
        'slug',
        'title',
        'author_name',
        'author_email',
        'author_url',
        'image',
        'content',
        'summary',
        'page_title',
        'description',
        'meta_tags',
        'json_meta_tags',
        'display_full_content_in_feed',
        'page_id'
    ];

    public function page()
    {
        return $this->belongsTo('App\Models\Page');
    }
}
