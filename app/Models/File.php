<?php

namespace App\Models;

use App\Exceptions\Models\RequestException;
use App\Mail\RequestShipped;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Support\Facades\Mail;

/**
 * Class File
 * @package App\Models
 */
class File extends Model
{
    use CrudTrait;

    protected $table = 'files';

    protected $fillable = [
        'user_id', 'block_name', 'file_name',
        'content', 'fileable_id', 'fileable_type'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function fileable()
    {
        return $this->morphTo();
    }

}
