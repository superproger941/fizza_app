<?php

namespace App\Models;

use App\Exceptions\Models\RequestException;
use App\Mail\RequestShipped;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Support\Facades\Mail;

/**
 * Class UserQuestion
 * @package App\Models
 */
class UserQuestion extends Model
{
    use CrudTrait;

    protected $table = 'user_questions';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id', 'question_id', 'answer'
    ];

    /**
     * Get all of the post's comments.
     */
    public function files()
    {
        return $this->morphMany('App\Models\File', 'fileable');
    }


}
