<?php

namespace App;

use App\Exceptions\Models\RequestException;
use App\Models\File;
use App\Models\Question;
use App\Models\UserQuestion;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
        'address', 'orgn', 'company_name',
        'inn', 'number', 'term',
        'sum', 'custom_data', 'base64_files'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get all of the post's comments.
     */
    public function files()
    {
        return $this->morphMany('App\Models\File', 'fileable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function questions()
    {
        return $this->belongsToMany(
            'App\Models\Question',
            'user_questions'
        );
    }

    /**
     *
     */
    public static function createUser(Request $request)
    {
        try {
            DB::transaction(function () use ($request) {
                $user = User::where(['email' => $request->email])->first() ?? new User();
                $user->name = $request->username ?? '' . rand();
                $user->email = $request->email;
                $user->address = $request->address;
                $user->ogrn = $request->ogrn;
                $user->company_name = $request->companyName;
                $user->inn = $request->inn;
                $user->number = $request->number;
                $user->term = $request->term;
                $user->sum = $request->sum;
                $password = str_random(16);
                $user->password = Hash::make($password);
                if ($user->save()) {
                    $email = $request->email;
                    Mail::raw('Ваш пароль: ' . $password,
                        function ($message) use ($email) {
                            $message->to($email)
                                ->from('mail@fizza.ru', 'Займы')
                                ->subject('Письмо с паролем.');
                        });
                }
                \Auth::login($user, true);
            });


        } catch (\Exception $e) {
            throw new RequestException($e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return static
     */
    public static function getUser(int $id): self
    {
        $user = User::where(['id' => $id])->first();

        return $user;
    }

    /**
     * @param Request $request
     * @throws RequestException
     */
    public static function updateUser(Request $request)
    {
        try {
            DB::transaction(function () use ($request) {
                $user = User::where(['id' => \Auth::user()->id])
                    ->first();
                $user->custom_data = json_encode($request->all());
                $user->save();
            });


        } catch (\Exception $e) {
            throw new RequestException($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @throws RequestException
     */
    public static function uploadFile(Request $request)
    {
        try {
            DB::transaction(function () use ($request) {
                $file = new File();
                $file->user_id = \Auth::user()->id;
                $file->block_name = $request->blockName;
                $file->file_name = $request->name;
                $file->content = $request->image;
                $file->save();
                $user = User::where(['id' => \Auth::user()->id])->first();
                $user->files()->save($file);
            });
        } catch (\Exception $e) {
            throw new RequestException($e->getMessage());
        }
    }

    public static function deleteUserFile(Request $request)
    {
        try {
            DB::transaction(function () use ($request) {
                $file = File::where([
                    'user_id' => \Auth::user()->id,
                    'block_name' => $request->blockName,
                    'file_name' => $request->name,

                ])->delete();
            });
        } catch (\Exception $e) {
            throw new RequestException($e->getMessage());
        }
    }

    /**
     * @return false|string
     */
    public static function getUserFiles(): string
    {
        $user = User::where(['id' => \Auth::user()->id])
            ->first();
        $arr = $user->files;
        $arr = $arr->toArray();

        return json_encode($arr);
    }

    /**
     * @return mixed
     */
    public static function getQuestions()
    {
        $questions = Question::where([
            'user_id' => \Auth::user()->id
        ])->with('answers')
            ->get();

        return $questions;
    }

    /**
     * @return array
     */
    public static function getUsersForSelect()
    {
        $users = self::all();
        $newUsers = [];
        foreach ($users as $user) {
            $newUsers[$user->id] = $user->name;
        }

        return $newUsers;
    }
}
