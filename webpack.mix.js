const mix = require('laravel-mix');

let productionSourceMaps = false;

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sourceMaps(productionSourceMaps, 'source-map');
/*if (!mix.inProduction()) {
    mix.webpackConfig({
        devtool: 'inline-source-map'
    })
}*/
