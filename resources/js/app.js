/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
let VueCookie = require('vue-cookie');
import store from './store/index';
import VueSweetalert2 from 'vue-sweetalert2';
import FirstStepRegistrationInfo from './components/FirstStep/RegistrationInfo';
import SecondStepRegistrationInfo from './components/SecondStep/RegistrationInfo';
import Router from 'vue-router';

window.Vue.use(Router);

window.Vue.use(VueCookie);
const options = {
    confirmButtonColor: '#ea5351'
};
window.Vue.use(VueSweetalert2, options);

Vue.component(
    'top-main-page-ajax-request-component',
    require('./components/Main/TopMainPage.vue').default
);
Vue.component(
    'middle-main-page-ajax-request-component',
    require('./components/Main/Middle.vue').default
);
Vue.component(
    'bottom-main-page-ajax-request-component',
    require('./components/Main/BottomMainPage.vue').default
);
Vue.component(
    'goals-main-page-ajax-request-component',
    require('./components/Main/Goals.vue').default
);
Vue.component(
    'main-page-faq',
    require('./components/Main/MainPageFaq.vue').default
);
Vue.component(
    'main-reviews',
    require('./components/Main/MainPageReviews.vue').default
);
Vue.component(
    'main-blogs',
    require('./components/Main/MainBlogs.vue').default
);

Vue.component(
    'step-steps',
    require('./components/Steps/Steps.vue').default
);

let router = new Router({
    routes: [
        {
            path: '/',
            name: 'main',
            component: FirstStepRegistrationInfo
        },
        {
            path: '/first-step-registration-info',
            name: 'first-step-registration-info',
            component: FirstStepRegistrationInfo,
            beforeEnter: (to, from, next) => {
                axios.post('/is-auth', {})
                    .then(function (response) {
                        if (response.data) {
                            next(false);
                        }
                        next();
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        },
        {
            path: '/second-step-registration-info',
            name: 'second-step-registration-info',
            component: SecondStepRegistrationInfo,
            beforeEnter: (to, from, next) => {
                axios.post('/is-auth', {})
                    .then(function (response) {
                        if (response.data === false) {
                            next('/')
                        }
                        next();
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        }
    ]
});


Vue.component(
    'loan-page-header-button',
    require('./components/LoanPage/HeaderButton.vue').default
);
Vue.component(
    'loan-page-map-block',
    require('./components/LoanPage/MapBlock.vue').default
);
Vue.component(
    'loan-page-steps-block',
    require('./components/LoanPage/StepsBlock.vue').default
);
Vue.component(
    'loan-page-about-block',
    require('./components/LoanPage/About.vue').default
);
Vue.component(
    'loan-page-main-block',
    require('./components/LoanPage/Main.vue').default
);
Vue.component(
    'loan-page-calc-block',
    require('./components/LoanPage/Calc.vue').default
);
Vue.component(
    'loan-page-mortgage-block',
    require('./components/LoanPage/Mortgage.vue').default
);
Vue.component(
    'loan-page-advantages-block',
    require('./components/LoanPage/Advantages.vue').default
);
Vue.component(
    'loan-page-faq',
    require('./components/LoanPage/LoanPageFaq.vue').default
);
Vue.component(
    'loan-reviews',
    require('./components/LoanPage/LoanPageReviews.vue').default
);
Vue.component(
    'loan-blogs',
    require('./components/LoanPage/LoanBlogs.vue').default
);

Vue.component(
    'blog-list-list',
    require('./components/Blog/List/List.vue').default
);
Vue.component(
    'blog-list-right-block',
    require('./components/Blog/RightBlock/RightBlock.vue').default
);

Vue.component(
    'callback',
    require('./components/Callback.vue').default
);

const app = new Vue({
    router,
    store
}).$mount('#app');
