<?php

return [
    'latest_entries' => 'Последние посты',
    'authors' => 'Авторы',
    'read_entry' => 'ЧИТАТЬ ПОСТЫ',
    'next' => 'СЛЕДУЮЩИЙ',
    'previous' => 'ПРЕДЫДУЩИЙ',
    'ads' => 'РЕКЛАМА',
];
