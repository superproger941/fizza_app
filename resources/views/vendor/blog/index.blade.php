@section('title', 'Блог')
@include($blog->bladeView('entry.header'))
<body>
<div class="mobile-menu">
    <div class="mobile-menu__close"></div>
    <div class="header__personal">
        <a href="#modal-info-callback" class="header-call">
            <img src="/img/design/ico/call-answer.svg" alt="img">
        </a>
        <a href="#" class="header-personal">
            <img src="/img/design/ico/user.svg" alt="img">
            Личный кабинет
        </a>
    </div>
    <div class="header__nav">
        <ul>
            <li><a href="{{url('/')}}">Займы бизнесу</a></li>
            <li><a href="{{url('loan')}}">Займы под залог</a></li>
            <li><a href="{{url('blog')}}">Блог</a></li>
        </ul>
    </div>
</div>
<!-- /.mobile-menu -->
<div class="header">
    <div class="container">
        <div class="header-wrap">
            <a href="{{url('/')}}">
                <div class="header__logo">
                    <img src="/img/design/logo.svg" alt="img">
                </div>
            </a>
            <!-- /.header__logo -->
            <div class="header__nav header__nav--lg">
                <ul>
                    <li><a href="{{url('/')}}">Займы бизнесу</a></li>
                    <li><a href="{{url('loan')}}">Займы под залог</a></li>
                    <li><a href="{{url('blog')}}">Блог</a></li>
                </ul>
            </div>
            <!-- /.header__nav -->
            <div class="header__personal header__personal--lg">
                <a href="#modal-info-callback" class="header-call">
                    <img src="/img/design/ico/call-answer.svg" alt="img">
                </a>
                <!-- /.header-call -->
                <a href="#" class="header-personal">
                    <img src="/img/design/ico/user.svg" alt="img">
                    Личный кабинет
                </a>
                <!-- /.header-personal -->
            </div>
            <!-- /.header__personal -->
            <div class="header__menu-open">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
</div>

<div class="container zerogrid">
    <div class="col-2-3" id="post-container">
        <div class="wrap-col">
            @if($entries->count())
                @each($blog->bladeView('entry.listItem'), $entries, 'entry')
                <div class="spacing-20"></div>
                <div class="clear"></div>
            @endif
        </div>
    </div>
    <div class="col-1-3">
        <div class="wrap-col">
            <div class="widget-container"><h6 class="widget-title">Категории</h6>
                <?php $categories = \App\Models\BlogCategory::getCategoriesForSelect()?>
                <ul>
                    <?php foreach($categories as $key => $category): ?>
                    {{--<li class="cat-item cat-item-5">--}}
                        {{--<a href="#"--}}
                           {{--title="View all posts filed under Apps">--}}
                            {{--{{$category}}--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    <?php endforeach;?>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="widget-container">
                <h6 class="widget-title">
                    Последние публикации
                </h6>
                <ul class="widget-recent-posts">
                    @if($entries->count())
                        @each($blog->bladeView('entry.latestPost'), $entries, 'entry')
                    @endif
                </ul>
                <!-- End Widget -->
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>        <!-- End Sidebar -->
    <div class="clear"></div>
</div>
@include($blog->bladeView('entry.footer'))
<callback :action="'create-request'"></callback>
</body>
</html>
