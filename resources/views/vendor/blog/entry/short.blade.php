<style>
    li {
        list-style-type: none;
    }
</style>
<!-- Start Post Item -->
<div class="post">
    <div class="post-margin">
        <h4 class="post-title">
            <a href="{{ $blog->urlToEntry($entry) }}" itemprop="url">
                {{ $entry->getTitle() }}
            </a>
        </h4>
        <ul class="post-status">
            <li><i class="fa fa-clock-o"></i>
                <time
                        datetime="{{ $blog->convertToBlogTimezone($entry->getPublished())->toAtomString() }}"
                        lang="ru" dir="ltr" itemprop="datePublished">
                    {{ $entry->getPublished()->diffForHumans() }}
                </time>
            </li>
            <li><i class="fa fa-folder-open-o"></i>
                <?php $categories = \App\Models\BlogCategory::getCategoriesForSelect()?>
                <a href="#" title="Смотреть посты"
                   rel="category">{{ $categories[$entry->getBlog()] }}</a></li>
            <li><i class="fa fa-comment-o"></i>Нет комментариев</li>
        </ul>
        <div class="clear"></div>
    </div>
    <div class="featured-image">
        {{ $entry->getImage() }}
        <div class="post-icon">
                    <span class="fa-stack fa-lg">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
                    </span>
        </div>
    </div>
    <div class="post-margin">
        <p>
            {{ $entry->getPartOfSummary() }}
        </p>
    </div>
    <ul class="post-social">
        <li>
            <a href="{{ $blog->urlToEntry($entry) }}" class="readmore">
                Читать <i class="fa fa-arrow-circle-o-right"></i>
            </a>
        </li>
    </ul>
    <div class="clear"></div>
</div>
<br>
