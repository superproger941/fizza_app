<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
<li><a href="{{ backpack_url('review') }}"><i class="fa fa-files-o"></i> <span>Отзывы</span></a></li>
<li><a href="{{ backpack_url('blogentry') }}"><i class="fa fa-files-o"></i> <span>Блог</span></a></li>
<li><a href="{{ backpack_url('request') }}"><i class="fa fa-files-o"></i> <span>Заявки</span></a></li>
<li><a href="{{ backpack_url('faq') }}"><i class="fa fa-files-o"></i> <span>Вопросы</span></a></li>

<li><a href='{{ backpack_url('question') }}'><i class='fa fa-question'></i> <span>Questions</span></a></li>