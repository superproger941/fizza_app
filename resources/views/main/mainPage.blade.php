@extends('layouts.front')
@section('title', 'Займы для бизнеса онлайн. Бизнес кредит без залога - ФИЗЗА.РУ')
@section('css')
    <link rel="stylesheet" href="/css/assets.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/secondstep/style.css">
    <style>
        .triangle_down1 {
            border-right: 0.2em solid black;
            border-top: 0.2em solid black;
        }
        .logo {
            color: black;
        }
    </style>
@endsection
@section('content')
    <main class="main">
        <div class="container">
            <div class="row">
                <div class="main__text">
                    <h1 class="main-title">
                        <span>Займы</span> для бизнеса
                    </h1>
                    <!-- /.main-title -->
                    <p class="main-description">
                        <span>Онлайн займы от 1 дня</span><br>
                        <small>
                            Кредит для бизнеса по всей РФ! С залогом и без.
                        </small>
                    </p>
                    <!-- /.main-description -->
                    <div class="main-about">
                        <div class="main-about-item">
                            <div class="main-about-item__title">
                                сумма<br>
                                займа до
                            </div>
                            <!-- /.main-about-item__title -->
                            <div class="main-about-item__info">
                                30 млн.
                            </div>
                            <!-- /.main-about-item__info -->
                        </div>
                        <!-- /.main-about-item -->
                        <div class="main-about-item">
                            <div class="main-about-item__title">
                                срок<br>
                                финансирования до
                            </div>
                            <!-- /.main-about-item__title -->
                            <div class="main-about-item__info">
                                6 мес.
                            </div>
                            <!-- /.main-about-item__info -->
                        </div>
                        <!-- /.main-about-item -->
                        <div class="main-about-item">
                            <div class="main-about-item__title">
                                ставка в<br>
                                месяц
                            </div>
                            <!-- /.main-about-item__title -->
                            <div class="main-about-item__info">
                                от 2,5%
                            </div>
                        </div>
                    </div>
                    <top-main-page-ajax-request-component
                            :action="'/steps'"
                            :class="'main-form main-form--lg'"
                    >
                    </top-main-page-ajax-request-component>
                </div>
                <div class="main__video main-video">
                    <div class="main-video__img">
                        <img src="/img/design/bg/main.png" alt="img">
                    </div>
                    <div class="main-video__btn">
                        <div class="video-btn">
                            <a href="#modal-video" class="video-btn__play open-modal"></a>
                            <!-- /.video-btn__play -->
                            <div class="video-btn__title">
                                Как это работает
                            </div>
                        </div>
                    </div>
                </div>
                <top-main-page-ajax-request-component
                    :action="'/steps'"
                    :class="'main-form main-form--xs'"
                >
                </top-main-page-ajax-request-component>
            </div>
        </div>
    </main>

    <section class="section about">
        <div class="container">
            <h2 class="section-header section-header--center">
					<span class="section-header__text">
						Бизнес займы. Просто и<br>
						быстро - все онлайн!
					</span>
                <!-- /.section-header__text -->
                <span class="section-header__bg">
						Просто
					</span>
                <!-- /.section-header__bg -->
            </h2>
            <!-- /.section-header -->
            <div class="about-wrap">
                <div class="row">
                    <div class="about-item">
                        <div class="about-item__ico">
                            <img src="/img/design/ico/startup.svg" alt="img">
                        </div>
                        <!-- /.about-item__ico -->
                        <h3 class="about-item__title">
                            Скорость решения
                        </h3>
                        <!-- /.about-item__title -->
                        <p class="about-item__text">
                            Предварительное решение
                            от 10 минут. Рассмотрение
                            и выдача онлайн.
                        </p>
                        <!-- /.about-item__text -->
                    </div>
                    <!-- /.about-item -->
                    <div class="about-item">
                        <div class="about-item__ico">
                            <img src="/img/design/ico/time.svg" alt="img">
                        </div>
                        <!-- /.about-item__ico -->
                        <h3 class="about-item__title">
                            Быстрое оформление
                        </h3>
                        <!-- /.about-item__title -->
                        <p class="about-item__text">
                            Быстрое решение после
                            предоставления минимального
                            пакета документов.
                        </p>
                        <!-- /.about-item__text -->
                    </div>
                    <!-- /.about-item -->
                    <div class="about-item">
                        <div class="about-item__ico">
                            <img src="/img/design/ico/money-bag.svg" alt="img">
                        </div>
                        <!-- /.about-item__ico -->
                        <h3 class="about-item__title">
                            Моментальные деньги
                        </h3>
                        <!-- /.about-item__title -->
                        <p class="about-item__text">
                            Деньги поступают на ваш
                            расчетный счет от 2-х часов
                            после подписания документов.
                        </p>
                        <!-- /.about-item__text -->
                    </div>
                    <!-- /.about-item -->
                    <div class="about-item">
                        <div class="about-item__ico">
                            <img src="/img/design/ico/like.svg" alt="img">
                        </div>
                        <!-- /.about-item__ico -->
                        <h3 class="about-item__title">
                            Надежный партнер
                        </h3>
                        <!-- /.about-item__title -->
                        <p class="about-item__text">
                            Привлекли финансирование
                            для компания по всей РФ на
                            сумму свыше 700 млн. руб.
                        </p>
                        <!-- /.about-item__text -->
                    </div>
                    <!-- /.about-item -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.about-wrap -->
        </div>
        <!-- /.container -->
    </section>
    <goals-main-page-ajax-request-component :action="'create-request'">
    </goals-main-page-ajax-request-component>
    <!-- /.section goals -->
    <section class="section advantages">
        <div class="container">
            <div class="advantages-item advantages-item--1">
                <div class="advantages-item__text advantages-item-text">
                    <h2 class="section-header">
                        Преимущества
                    </h2>
                    <!-- /.section-header -->
                    <h3>
                        Выгодно!
                    </h3>
                    <p>
                        <b>Сроки от 1 до 6 месяцев</b> с возможностью пролонгации
                    </p>
                    <p>
                        <b>Займы для бизнеса с минимальной переплатой.</b> Займы на участие в тендере и на исполнение
                        госконтакта, закрыть временный кассовый разрыв, пополнение оборотных средств, покупка основных
                        средств, на иные цели.
                    </p>
                    <p>
                        <b>Ставка от 2,5% в месяц.</b> Досрочное погашение без штрафов с пересчетом процентов в вашу
                        пользу.
                    </p>
                    <p>
                        <b>Сумма займа до 30 млн.</b>
                    </p>
                    <p>
                        Улучшите условия будущих займов. Нет просрочек в платежах? Сервис будет заинтересован увеличить
                        сумму и срок, а также снизить процентную ставку.
                    </p>
                    <div class="advantages-item-text__bg">
                        Выгодно
                    </div>
                    <!-- /.advantages-item-text__bg -->
                </div>
                <!-- /.advantages-item__text -->
                <div class="advantages-item__img">
                    <div class="advantages-item-img advantages-item-img--1">
                        <div class="advantages-item-img__photo">
                            <img src="/img/design/advantages/1.png" alt="img">
                        </div>
                        <!-- /.advantages-item-img__photo -->
                        <div class="advantages-item-img__element">
                            <img src="/img/design/elements/advantages-1.svg" alt="img">
                        </div>
                        <!-- /.advantages-item-img__element -->
                    </div>
                    <!-- /.advantages-item-img -->
                </div>
                <!-- /.advantages-item__img -->
            </div>
            <!-- /.advantages-item -->
            <div class="advantages-item advantages-item--2">
                <div class="advantages-item__img">
                    <div class="advantages-item-img advantages-item-img--2">
                        <div class="advantages-item-img__photo">
                            <img src="/img/design/advantages/2.png" alt="img">
                        </div>
                        <!-- /.advantages-item-img__photo -->
                        <div class="advantages-item-img__element">
                            <img src="/img/design/elements/advantages-2.svg" alt="img">
                        </div>
                        <!-- /.advantages-item-img__element -->
                    </div>
                    <!-- /.advantages-item-img -->
                </div>
                <div class="advantages-item__text advantages-item-text">
                    <h3>
                        Быстро!
                    </h3>
                    <p>
                        <b>Займы бизнесу онлайн без банков</b><br>
                        Сервис финансивароняи бизнеса. Инвесторы и партнеры выдают займы юридическим лицам (компаниям) –
                        представителям малого и среднего бизнеса. Без банков, быстро и легко!
                    </p>
                    <p>
                        <b>Решение по займу через от 15 минут</b><br>
                        Банки закрывая глаза на целые отрасли, используя строгие методы оценки компаний. Сервис в свою
                        очередь, имеет более быстрый и технологичный подход. Решение по вашему займу занимает всего от
                        15 минут.
                    </p>
                    <p>
                        <b>Деньги на вашем счете от 1 дня</b><br>
                        Минимальный срок получения денег – 1 день. От 60 минут на проверку заявки. От 1 дня на
                        подготовку сделки и перевод средств вам на расчетный счет.
                    </p>
                    <div class="advantages-item-text__bg">
                        Быстро
                    </div>
                    <!-- /.advantages-item-text__bg -->
                </div>
                <!-- /.advantages-item__text -->
            </div>
            <!-- /.advantages-item -->
            <div class="advantages-item advantages-item--3">
                <div class="advantages-item__text advantages-item-text">
                    <h3>
                        Легко! Онлайн!
                    </h3>
                    <p>
                        <b>Бизнес заём (кредит) до 30 млн руб.!</b><br>
                        Нет банковских сложностей: займы для компаний, всё онлайн по всей РФ
                    </p>
                    <p>
                        <b>Без посещения, оформление online</b><br>
                        Минимальный пакет документов, с залогом и без, никуда не нужно ездить и всё через интернет.
                    </p>
                    <p>
                        <b>Деньги на счете от 1 дня</b><br>
                        Компания заключает договор займа в электронном виде с инвестором или партнером сервиса. Мы
                        подготавливаем документы для быстрого онлайн подписания займа. В каждый договор мы автоматически
                        подставляет реквизиты сторон и условия займа. Поэтому процедура подписания пакета договоров
                        проходит для компании оптимально быстро и без лишних хлопот.
                    </p>
                    <div class="advantages-item-text__bg">
                        Легко
                    </div>
                    <!-- /.advantages-item-text__bg -->
                </div>
                <!-- /.advantages-item__text -->
                <div class="advantages-item__img">
                    <div class="advantages-item-img advantages-item-img--3">
                        <div class="advantages-item-img__photo">
                            <img src="/img/design/advantages/3.png" alt="img">
                        </div>
                        <!-- /.advantages-item-img__photo -->
                        <div class="advantages-item-img__element">
                            <img src="/img/design/elements/advantages-3.svg" alt="img">
                        </div>
                        <!-- /.advantages-item-img__element -->
                    </div>
                    <!-- /.advantages-item-img -->
                </div>
                <!-- /.advantages-item__img -->
            </div>
            <!-- /.advantages-item -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /.section advantages -->
    <section class="section steps">
        <div class="container">
            <h2 class="section-header section-header--white section-header--center">
                Займы для бизнеса онлайн за 4 шага
            </h2>
            <!-- /.section-header -->
            <div class="steps-list">
                <div class="steps-list-row">
                    <div class="steps-list-item steps-list-item--left">
                        <div class="steps-list-item__title">
                            Заполните заявку
                        </div>
                        <!-- /.steps-list-item__title -->
                        <div class="steps-list-item__text">
                            <a href="#">Оставьте заявку</a> на сайте. Требуется всего з<br>
                            минуты вашего времени и ИНН организации.<br>
                            В течении 30 минут вы узнаете<br>
                            предварительное решение по заявке.
                        </div>
                        <!-- /.steps-list-item__text -->
                        <div class="steps-list-item__marker">01</div>
                        <!-- /.steps-list-item__marker -->
                    </div>
                    <!-- /.steps-list-item -->
                </div>
                <!-- /.steps-list-row -->
                <div class="steps-list-row">
                    <div class="steps-list-item steps-list-item--right">
                        <div class="steps-list-item__title">
                            Предоставьте документы
                        </div>
                        <!-- /.steps-list-item__title -->
                        <div class="steps-list-item__text">
                            После того, как вы получите одобрение<br>
                            займа, требуется предоставить<br>
                            документы компании для детальной оценке.
                        </div>
                        <!-- /.steps-list-item__text -->
                        <div class="steps-list-item__marker">02</div>
                        <!-- /.steps-list-item__marker -->
                    </div>
                    <!-- /.steps-list-item -->
                </div>
                <!-- /.steps-list-row -->
                <div class="steps-list-row">
                    <div class="steps-list-item steps-list-item--left">
                        <div class="steps-list-item__title">
                            Подпишите договор онлайн
                        </div>
                        <!-- /.steps-list-item__title -->
                        <div class="steps-list-item__text">
                            Будут оговорены все условия<br>
                            выдачи бизнес займа и подписан договор.
                        </div>
                        <!-- /.steps-list-item__text -->
                        <div class="steps-list-item__marker">03</div>
                        <!-- /.steps-list-item__marker -->
                    </div>
                    <!-- /.steps-list-item -->
                </div>
                <!-- /.steps-list-row -->
                <div class="steps-list-row">
                    <div class="steps-list-item steps-list-item--right steps-list-item--last">
                        <div class="steps-list-item__title">
                            Получите деньги
                        </div>
                        <!-- /.steps-list-item__title -->
                        <div class="steps-list-item__text">
                            После того, как договор будет подписан<br>
                            вы получите деньги. Они поступят на<br>
                            расчетный счет в течении одного<br>
                            банковского дня.
                        </div>
                        <div class="steps-list-item__marker">04</div>
                    </div>
                </div>
            </div>
            <middle-main-page-ajax-request-component
                    :action="'/create-request'"
            >

            </middle-main-page-ajax-request-component>
        </div>
    </section>
    <section class="section popular">
        <div class="container">
            <div class="row">
                <div class="popular__title">
                    <h2 class="section-header section-header--white">
                        Популярные<br>
                        цели для<br>
                        финансирования<br>
                        компаний
                    </h2>
                    <!-- /.section-header -->
                    <img src="/img/design/elements/popular.png" alt="img">
                </div>
                <!-- /.popular__title -->
                <div class="popular__list">
                    <ul>
                        <li>
                            Заём на исполнение контрактов 44-ФЗ, 223-ФЗ
                        </li>
                        <li>
                            Покрытие кассового разрыва, в связи с отсрочкой платежа
                        </li>
                        <li>
                            Покупка оборудования и материалов для производства
                        </li>
                        <li>
                            Открытие новой точки продаж или представительства компании
                        </li>
                        <li>
                            Оплата поставок
                        </li>
                        <li>
                            Займы под залог автомобиля, коммерческого транспорта или недвижимости
                        </li>
                        <li>
                            Покупка коммерской недвижимости
                        </li>
                    </ul>
                </div>
                <!-- /.popular__list -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /.section popular -->
    <main-reviews>
    </main-reviews>
    <section class="section form-question">
        <div class="container">
            <div class="row">
                <div class="form-question__form">
                    <h2 class="section-header section-header--center">
                        Остались вопросы?
                    </h2>
                    <div class="form-question-form">
                        <bottom-main-page-ajax-request-component
                                :action="'/create-request'">
                        </bottom-main-page-ajax-request-component>
                        <div class="form-question-form__privacy">
                            Нажимая кнопку «Отправить заявку», я соглашаюсь с условиями<br>
                            <a href="#">пользовательского соглашения</a>, даю согласие на обработку<br>
                            персональных данных и соглашаюсь c <a href="#">политикой конфиденциальности</a>.
                        </div>
                    </div>
                </div>
                <div class="form-question__img">
                    <img src="/img/content/form-question.png" alt="img">
                </div>
            </div>
        </div>
    </section>
    <main-blogs>
    </main-blogs>
    <section class="section quote">
        <div class="container">
            <div class="row">
                <div class="quote__persona">
                    <div class="quote-persona">
                        <div class="quote-persona__photo">
                            <img src="/img/content/persona.png" alt="img">
                        </div>
                        <!-- /.quote-persona__photo -->
                        <div class="quote-persona__title quote-persona-title">
                            <div class="quote-persona-title__name">
                                Малиновский<br>
                                Владимир
                            </div>
                            <!-- /.quote-persona-title__name -->
                            <div class="quote-persona-title__who">
                                Основатель fizza.ru
                            </div>
                            <!-- /.quote-persona-title__who -->
                        </div>
                        <!-- /.quote-persona__title -->
                    </div>
                    <!-- /.quote-persona -->
                </div>
                <!-- /.quote__persona -->
                <div class="quote__text">
                    <h2 class="section-header section-header--small">
                        Деньги для бинеса, когда это по-настоящему важно!
                    </h2>
                    <!-- /.section-header -->
                    <p>
                        Сервис финансирования «FIZZA.RU» оформляет займы для бизнеса с минимальными требованиями к
                        заемщику, выдавая займы от инвесторов и небанковских финансовых компаний по принципу
                        Краудлендинга (взаимного финансирования). Займы для бизнеса выдаются на основании оценки
                        компании, которая осуществляется по банковской выписке и небольшому пакету документов.
                    </p>
                    <p>
                        У заявителя не должно быть негативной кредитной истории и крупных долгов, которые могут привести
                        к банкротству. С помощью нашего сервиса частные инвесторы и финансовые компании, получают
                        возможность финансировать малый бизнес. Таким образом, предприниматели могут получать быстрые
                        деньги в обход бюрократических барьеров, а инвесторы получают выгодные вложения капитала.
                    </p>
                    <!-- /.btn -->
                </div>
                <!-- /.quote__text -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <main-page-faq>
    </main-page-faq>
@endsection

@section('js')
    <script type='text/javascript' src='/js/vendor/jquery-3.2.1.min.js'></script>
    <script src="/js/vendor/bootstrap.min.js"></script>
    <script src="/js/assets.js?v=258760696"></script>
    <script src="/js/main.js?v=258760696"></script>
    <script type="text/javascript" charset="utf-8" async
            src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ad43564f76ac8dc93733d06b92290ed343fc62cb82cebc007e48e2e04f97d1376&amp;width=100%25&amp;height=500&amp;id=map&amp;lang=ru_RU&amp;scroll=true"></script>
@endsection
