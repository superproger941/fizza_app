@extends('layouts.front')
@section('title', 'Займы под залог')
@section('css')
    <link rel="stylesheet" href="/css/assets.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/loan.css">
@endsection
@section('content')
    <loan-page-main-block>
    </loan-page-main-block>
    <loan-page-about-block>
    </loan-page-about-block>
    <loan-page-calc-block :action="'/steps'">
    </loan-page-calc-block>
    <loan-page-mortgage-block>
    </loan-page-mortgage-block>
    <loan-page-map-block :action="'/create-request'">
    </loan-page-map-block>
    <loan-page-steps-block :action="'/steps'">
    </loan-page-steps-block>
    <loan-page-advantages-block>
    </loan-page-advantages-block>
    <loan-reviews>
    </loan-reviews>
    <section class="section form-question">
        <div class="container">
            <div class="row">
                <div class="form-question__form">
                    <h2 class="section-header section-header--center">
                        Остались вопросы?
                    </h2>

                    <div class="form-question-form">
                        <bottom-main-page-ajax-request-component
                            :action="'/create-request'">
                        </bottom-main-page-ajax-request-component>
                        <div class="form-question-form__privacy">
                            Нажимая кнопку «Отправить заявку», я соглашаюсь с условиями<br><a href="#">пользовательского
                                соглашения</a>, даю согласие на обработку<br>
                            персональных данных и соглашаюсь c <a href="#">политикой конфиденциальности</a>.
                            </a>
                        </div>

                    </div>

                </div>

                <div class="form-question__img">
                    <img src="img/content/form-question.png" alt="img">
                </div>

            </div>

        </div>

    </section>
    <loan-blogs>
    </loan-blogs>
    <section class="section quote">
        <div class="container">
            <div class="row">
                <div class="quote__persona">
                    <div class="quote-persona">
                        <div class="quote-persona__photo">
                            <img src="img/content/persona.png" alt="img">
                        </div>

                        <div class="quote-persona__title quote-persona-title">
                            <div class="quote-persona-title__name">
                                Малиновский<br>
                                Владимир
                            </div>

                            <div class="quote-persona-title__who">
                                Основатель fizza.ru
                            </div>

                        </div>

                    </div>

                </div>

                <div class="quote__text">
                    <h2 class="section-header section-header--small">
                        Деньги под залог недвижимости, когда это по-настоящему нужно!
                    </h2>

                    <p>
                        Сервис финансирования «FIZZA.RU» оформляет займы под залог недвижимости с минимальными
                        требованиями к заемщику, выдавая займы от инвесторов и небанковских финансовых компаний по
                        принципу Краудлендинга (взаимного финансирования). Займы под залог недвижимости выдаются на
                        основании оценки и 2 документов за 1 час. Выдача наличных денег от 1 дня.
                    </p>
                    <p>
                        У заявителя может быть негативная кредитная история С помощью нашего сервиса частные инвесторы и
                        финансовые компании, получают возможность финансировать под залог недвижимости. Таким образом,
                        вы получаете быстрые деньги под залог недвижимости в обход бюрократических барьеров, а инвесторы
                        получают выгодные вложения капитала.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <loan-page-faq>
    </loan-page-faq>
@endsection
@section('js')
    <script type='text/javascript' src='/js/vendor/jquery-3.2.1.min.js'></script>
    <script src="/js/vendor/bootstrap.min.js"></script>
    <script src="/js/assets.js?v=258760696"></script>
    <script src="/js/main.js?v=258760696"></script>
    <script type="text/javascript" charset="utf-8" async
            src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ad43564f76ac8dc93733d06b92290ed343fc62cb82cebc007e48e2e04f97d1376&amp;width=100%25&amp;height=500&amp;id=map&amp;lang=ru_RU&amp;scroll=true"></script>
@endsection
