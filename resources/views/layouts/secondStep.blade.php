<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        @yield(
        'title',
         'Займы для бизнеса онлайн. Бизнес кредит без залога - ФИЗЗА.РУ'
         )
    </title>
    <meta name="description"
          content="Бизнес займы онлайн от 2%. На исполнение контрактов. На пополнение оборотных средств. Под залог авто и недвижимости. Бизнес кредит. Быстро и легко получите деньги для компании">
    <meta name="keywords"
          content="займы для бизнеса, бизнес заем, бизнес кредит, кредит для бизнеса, займы на исполнение контрактов, под залог авто, кредит бизнес">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('css')
</head>
<body>
<div class="mobile-menu">
    <div class="mobile-menu__close"></div>
    <!-- /.mobile-menu__close -->
    <div class="header__personal">
        <a href="#modal-info-callback" class="header-call">
            <img src="/img/design/ico/call-answer.svg" alt="img">
        </a>
        <!-- /.header-call -->
        <a href="#" class="header-personal">
            <img src="/img/design/ico/user.svg" alt="img">
            Личный кабинет
        </a>
        <!-- /.header-personal -->
    </div>
    <!-- /.header__personal -->
    <div class="header__nav">
        <ul>
            <li><a href="{{url('/')}}">Займы бизнесу</a></li>
            <li><a href="{{url('loan')}}">Займы под залог</a></li>
            <li><a href="{{url('blog')}}">Блог</a></li>
        </ul>
    </div>
    <!-- /.header__nav -->
</div>
<!-- /.mobile-menu -->
<div class="header">
    <div class="container">
        <div class="header-wrap">
            <a href="/">
                <div class="header__logo">
                    <img src="/img/design/logo.svg" alt="img">
                </div>
            </a>
            <!-- /.header__logo -->
            <div class="header__nav header__nav--lg">
                <ul>
                    <li><a href="{{url('/')}}">Займы бизнесу</a></li>
                    <li><a href="{{url('loan')}}">Займы под залог</a></li>
                    <li><a href="{{url('blog')}}">Блог</a></li>
                </ul>
            </div>
            <!-- /.header__nav -->
            <div class="header__personal header__personal--lg">
                <div class="logo">
                    <a href="#">
                    <span class="logo__icon">
                        <img class="red-block" src="images/secondstep/red-block.png">
                        <img src="images/secondstep/dialog-icon.png">
                    </span>
                    </a>
                    <a href="#">
               href="         ООО "ФИРМА"
                        <div id="dialog2" class="triangle_down1"></div>
                    </a>
                </div>
            </div>
            <!-- /.header__personal -->
            <div class="header__menu-open">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <!-- /.header__menu-open -->
        </div>
        <!-- /.header-wrap -->
    </div>
</div>

<div id="app" class="wrapper">
    @yield('content')
    <callback :action="'create-request'"></callback>
</div>

<div id="app" class="footer">
    <div class="container">
        <div class="footer-main">
            <div class="footer__logo">
                <img src="/img/design/logo-footer.svg" alt="img">
            </div>
            <!-- /.footer__logo -->
            <div class="footer__info">
                <ul>
                    <li><a href="#">Пользовательское соглашение</a></li>
                    <li><a href="#">Политика конфиденциальности</a></li>
                    <li><a href="#">Общие условия договора займа</a></li>
                </ul>
            </div>
            <!-- /.footer__info -->
            <div class="footer__contact footer-contact">
                <a href="tel:+74952666568" class="footer-contact__phone">+7 (495) 266-65-58</a><br>
                <!-- /.footer-contact__phone -->
                <a href="mailto:hello@fizza.ru" class="footer-contact__mail">hello@fizza.ru</a>
                <!-- /.footer-contact__mail -->
            </div>
            <!-- /.footer__contact -->
            <div class="footer__callback">
                <a href="#modal-info-callback" class="open-modal">
                    Обратный звонок
                </a>
            </div>
            <!-- /.footer__callback -->
        </div>
        <!-- /.footer-main -->
        <div class="footer__copyright">
            © 2017-2019, ООО «ФИЗЗА» ИНН 7703437703 Все права защищены. Деятельность сайта fizza.ru не подлежит
            лицензированию или регулированию какими-либо специализированными агентствами, организациями или
            государственными учреждениями. Данный интернет-ресурс носит исключительно информационно-справочный характер,
            и не является публичной офертой или предложением делать оферты, определяемых положениями ст. 435, 437
            Гражданского кодекса Российской Федерации. ООО «ФИЗЗА» использует файлы cookie с целью персонализации
            сервисов и повышения удобства пользования веб-сайтом. Cookie представляют собой небольшие файлы, содержащие
            информацию о предыдущих посещениях веб-сайта. Если вы не хотите использовать файлы cookie, измените
            настройки браузера.
            <a href="mailto:hello@fizza.ru">hello@fizza.ru</a> <a href="tel:+74952666568">+7 495 266-65-68</a>
        </div>
        <!-- /.footer__copyright -->
    </div>

    <div class="modal modal--video" style="background-color:none" id="modal-video">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/XXhYiIg6b-M" frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen></iframe>
    </div>

    <div class="responsive-marker">
        <div class="responsive-marker__item responsive-marker__item--lg">lg</div>
        <div class="responsive-marker__item responsive-marker__item--md">md</div>
        <div class="responsive-marker__item responsive-marker__item--sm">sm</div>
        <div class="responsive-marker__item responsive-marker__item--xs">xs</div>
    </div>

    <div class="resolution-marker"></div>

</div>
<script src="{{ asset('js/app.js') }}"></script>
@yield('js')
</body>
</html>
